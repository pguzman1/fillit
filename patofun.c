/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   patofun.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/09 15:42:17 by pguzman           #+#    #+#             */
/*   Updated: 2015/12/11 16:09:34 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	**ft_createboard(int i)
{
	char	**board;
	int		j;
	int		k;

	j = 0;
	board = (char **)malloc(sizeof(board) * (i + 1));
	while (j < i)
	{
		board[j] = (char *)malloc(sizeof(board[j]) * (i + 1));
		j++;
	}
	board[j] = NULL;
	j = 0;
	while (j < i)
	{
		k = 0;
		while (k < i) // MEMSEEEEEEEEEEEETTTTTTTTTTTT
			board[j][k++] = '.';
		board[j][k] = '\0';
		j++;
	}
	return (board);
}

void	ft_printboard(char **board)
{
	int i;
	int j;
	int len;

	i = 0;
	len = ft_strlen(board[0]);
	while (i < len)
	{
		j = 0;
		while (j < len)
		{
			ft_putchar(board[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

void		ft_put_in_board(t_piece tri, char **board, int i , int j)
{
	if (tri.first.x == 0 && tri.first.y == 0)
	{
		board[i + tri.first.x][j + tri.first.y] = tri.letter;
		board[i + tri.second.x][j + tri.second.y] = tri.letter;
		board[i + tri.third.x][j + tri.third.y] = tri.letter;
		board[i + tri.fourth.x][j + tri.fourth.y] = tri.letter;
	}
	else if (tri.first.x == 0 && tri.first.y == 1)
	{
		board[i][j] = tri.letter;//change
		board[i + tri.second.x][j + tri.second.y - 1] = tri.letter;
		board[i + tri.third.x][j + tri.third.y - 1] = tri.letter;
		board[i + tri.fourth.x][j + tri.fourth.y - 1] = tri.letter;
	}
	else
	{
		board[i][j] = tri.letter;//change
		board[i + tri.second.x][j + tri.second.y - 2] = tri.letter;
		board[i + tri.third.x][j + tri.third.y - 2] = tri.letter;
		board[i + tri.fourth.x][j + tri.fourth.y - 2] = tri.letter;
	}
}

int			ft_is_possible(char **board,  t_piece tri, int i, int j)
{
	int		len = ft_strlen(board[0]);

	if (tri.first.y + j >= len || tri.second.y + j >= len
			|| tri.third.y + j >= len || tri.fourth.y + j >= len
			|| tri.first.x + i >= len || tri.second.x + i >= len
			|| tri.third.x + i >= len || tri.fourth.x + i >= len)
		return (0);
	else if (j == 0 && (tri.first.y == 1 || tri.first.y == 2))
		return (0);
	else if (j == 1 && tri.first.y == 2) // Add
		return (0);
	else if (!ft_is_empty(board, tri, i, j))
 		return (0);
	else
		return (1);
}
/*	int		len = ft_strlen(board[0]);
	
	if (j == 0 && (tri.first.y == 1 || tri.first.y == 2))
		return (0);
	else if ((tri.fourth.x - tri.first.x == 3) && i >= len - 3)
		return (0);
	else if ((tri.fourth.y - tri.first.y == 3) && j >= len - 3 )
		return (0);
	else if (i == (len - 2))
	{
		if (tri.third.x > tri.first.x || tri.fourth.x > tri.first.x)
			return (0);
	}
		return (0);
	else if (i == (len - 1))
	{
		if ()
	}
		return (0);
	else if (ft_is_empty(board, tri, i, j))
		return (0);
	else
		return (1);*/

int		ft_fill(char **board, t_piece *tri)
{
	int i;
	int j;
	int len;

	len = ft_strlen(board[0]);
	i = 0;
	if (tri == NULL)
		return (1);
	while (i < len)
	{
		j = 0;
		while (j < len)
		{
			if (ft_is_possible(board, *tri, i, j))
			{
				ft_put_in_board(*tri, board, i, j);
				ft_printboard(board);
				if (ft_fill(board, tri->next))
					return (1);
				ft_remove_piece(board, *tri, i, j);
			}
			j++;
		}
		i++;
	}
	return (0);
}

void	ft_remove_piece(char **board, t_piece tri, int i, int j)
{
	if (tri.first.x == 0 && tri.first.y == 0)
	{
		board[i + tri.first.x][j + tri.first.y] = '.';
		board[i + tri.second.x][j + tri.second.y] = '.';
		board[i + tri.third.x][j + tri.third.y] = '.';
		board[i + tri.fourth.x][j + tri.fourth.y] = '.';
	}
	else if (tri.first.x == 0 && tri.first.y == 1)
	{
		board[i][j] = '.';
		board[i + tri.second.x][j + tri.second.y - 1] = '.';
		board[i + tri.third.x][j + tri.third.y - 1] = '.';
		board[i + tri.fourth.x][j + tri.fourth.y - 1] = '.';
	}
	else
	{
		board[i][j] = '.';
		board[i + tri.second.x][j + tri.second.y - 2] = '.';
		board[i + tri.third.x][j + tri.third.y - 2] = '.';
		board[i + tri.fourth.x][j + tri.fourth.y - 2] = '.';
	}

}

int		*ft_get_size(int n)
{
	int *a;
	int i;
	int j;

	a = (int*)malloc(sizeof(a) * 4);
	a[0] = 0;
	a[1] = 0;
	a[2] = 0;
	a[3] = 0;
	j = 0;
	i = 0;
	while (j < 3 && i < 14)
	{
		if (((i * i) >= (n * 4)) )
			a[j++] = i;
		i++;
	}
	return (a);
}

char		**ft_solve(t_piece *tri, int *poss)
{
	int i;
	char **result;

	i = 0;
	result = NULL;
	while (poss[i] != 0)
	{
		result = ft_createboard(poss[i]); 
		if (ft_fill(result, tri))
			return (result);
		i++;
	}
	return (result);
}
