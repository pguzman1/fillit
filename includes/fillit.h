/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cjacques <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 13:15:01 by cjacques          #+#    #+#             */
/*   Updated: 2015/12/14 16:30:02 by cjacques         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# define BUF_SIZE 21

# include "libft.h"
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>

typedef struct		s_point
{
	int				x;
	int				y;
}					t_point;

typedef struct		s_piece
{
	t_point			first;
	t_point			second;
	t_point			third;
	t_point			fourth;
	char			letter;
	struct s_piece	*next;
}					t_piece;

void				ft_find_error(void);
int					ft_open_file(char *path);
char				**ft_file_to_tab(char *path, int nb);
void				ft_verif_tetri(char **ptr, int nb);
t_piece				*ft_tab_to_lst(char **ptr, int nb);
void				ft_lst_del(t_piece **lst);
void				ft_str_del(char **str);
char				**ft_solve(t_piece *tri, int *poss);
int					*ft_get_size(int n);
void				ft_printboard(char **board);
int					ft_is_empty(char **board, t_piece tri, int i, int j);
char				**ft_createboard(int i);

#endif
