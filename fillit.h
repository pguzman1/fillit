/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cjacques <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 13:15:01 by cjacques          #+#    #+#             */
/*   Updated: 2015/12/10 16:48:46 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# define BUF_SIZE 21

# include "libft.h"
# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>

typedef struct		s_point
{
	int				x;
	int				y;
}					t_point;

typedef struct		s_piece
{
	t_point			first;
	t_point			second;
	t_point			third;
	t_point			fourth;
	char			letter;
	struct s_piece	*next;
}					t_piece;

void				ft_valid_character(char c);
void				ft_find_error(void);
int					ft_open_file(char *path);
int					ft_count_tetri(char c);
char				**ft_file_to_tab(char *path, int nb);
char				*ft_del_char(char *buf, char c);
void				ft_verif_tetri(char **ptr, int nb);
void				ft_schema(char schema[19][14]);
t_piece				*ft_create_tetri(char *ptr, int nb);
t_piece				*ft_tab_to_lst(char **ptr, int nb);
void				ft_lst_add_back(t_piece **begin_lst, t_piece *new);
t_piece				*ft_repack_tplt(t_piece *piece);
void				ft_lst_del(t_piece **lst);
void				ft_str_del(char **str);
int					ft_find_letter(char **board, t_piece tri);
int					ft_is_empty(char **board, t_piece tri, int i, int j);
void				ft_remove_piece(char **board, t_piece, int i, int j);
char				**ft_solve(t_piece *tri, int *poss);
int					*ft_get_size(int n);
int					ft_fill(char **board, t_piece *tri);
int					ft_is_possible(char **board,  t_piece tri, int i, int j);
void				ft_put_in_board(t_piece tri, char **board, int i , int j);
void				ft_printboard(char **board);
char				**ft_createboard(int i);

#endif
