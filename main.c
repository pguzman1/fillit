/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cjacques <cjacques@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 14:43:01 by cjacques          #+#    #+#             */
/*   Updated: 2015/12/15 09:47:19 by cjacques         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static t_piece		*ft_create_tetri(char *ptr, int nb)
{
	int			i;
	t_piece		*piece;

	i = -1;
	piece = (t_piece*)malloc(sizeof(*piece));
	(piece == NULL) ? ft_find_error() : 1;
	while (ptr[++i] != '#')
		;
	piece->first.x = i / 4;
	piece->first.y = i % 4;
	while (ptr[++i] != '#')
		;
	piece->second.x = i / 4;
	piece->second.y = i % 4;
	while (ptr[++i] != '#')
		;
	piece->third.x = i / 4;
	piece->third.y = i % 4;
	while (ptr[++i] != '#')
		;
	piece->fourth.x = i / 4;
	piece->fourth.y = i % 4;
	piece->letter = nb + 'A';
	piece->next = NULL;
	return (piece);
}

static void			ft_lst_add_back(t_piece **begin_lst, t_piece *new)
{
	t_piece		*lst;

	lst = *begin_lst;
	if (new == NULL)
		return ;
	if (lst)
	{
		while (lst->next)
			lst = lst->next;
		lst->next = new;
	}
	else
		*begin_lst = new;
}

static t_piece		*ft_repack_tplt(t_piece *piece)
{
	while (piece->first.x != 0 && piece->second.x != 0 && piece->third.x != 0
			&& piece->fourth.x != 0)
	{
		(piece->first.x)--;
		(piece->second.x)--;
		(piece->third.x)--;
		(piece->fourth.x)--;
	}
	while (piece->first.y != 0 && piece->second.y != 0 && piece->third.y != 0
			&& piece->fourth.y != 0)
	{
		(piece->first.y)--;
		(piece->second.y)--;
		(piece->third.y)--;
		(piece->fourth.y)--;
	}
	return (piece);
}

t_piece				*ft_tab_to_lst(char **ptr, int nb)
{
	int		i;
	t_piece	*lst_tetri;

	lst_tetri = NULL;
	i = 0;
	while (i < nb)
	{
		ft_lst_add_back(&lst_tetri, ft_repack_tplt(ft_create_tetri(ptr[i], i)));
		i++;
	}
	return (lst_tetri);
}

int					main(int ac, char **av)
{
	char	**ptr;
	int		nb_tetri;
	t_piece	*lst_tetri;

	if (ac != 2)
		ft_find_error();
	nb_tetri = ft_open_file(av[1]);
	ptr = ft_file_to_tab(av[1], nb_tetri);
	ft_verif_tetri(ptr, nb_tetri);
	lst_tetri = ft_tab_to_lst(ptr, nb_tetri);
	ft_printboard(ft_solve(lst_tetri, ft_get_size(nb_tetri)));
	ft_str_del(ptr);
	ft_lst_del(&lst_tetri);
	return (0);
}
