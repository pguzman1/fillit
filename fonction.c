/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fonction.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cjacques <cjacques@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 09:21:41 by cjacques          #+#    #+#             */
/*   Updated: 2015/12/15 15:19:58 by cjacques         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_find_error(void)
{
	ft_putstr("error\n");
	exit(-1);
}

static void	ft_valid_char(char c)
{
	static int	dot = 0;
	static int	htag = 0;
	static int	re_line = 0;

	if (c == '.')
	{
		dot++;
		(dot > 12) ? ft_find_error() : 1;
	}
	else if (c == '#')
	{
		htag++;
		(htag > 4) ? ft_find_error() : 1;
	}
	else
	{
		((htag + dot) % 4 != 0) ? ft_find_error() : 1;
		re_line++;
		if (re_line == 5 && htag == 4 && dot == 12)
		{
			re_line = 0;
			htag = 0;
			dot = 0;
		}
	}
}

static int	ft_count_tetri(char c)
{
	static int	count_char = 0;
	static int	retour = 0;
	static int	count_pcs = 0;

	count_pcs++;
	if (c == '.' || c == '#')
	{
		count_char++;
		retour = 0;
	}
	else
	{
		if (count_char != 4 && count_char != 0)
			ft_find_error();
		else
			count_char = 0;
		if (count_char == 0)
			retour++;
		if (retour > 2)
			ft_find_error();
	}
	if (count_pcs > 546)
		ft_find_error();
	return (count_pcs);
}

int			ft_open_file(char *path)
{
	int		fd;
	char	c;
	int		nb_char;
	int		nb_tetri;

	fd = open(path, O_RDONLY);
	if (fd == -1)
		ft_find_error();
	while (read(fd, &c, 1))
	{
		if (c != '.' && c != '#' && c != '\n')
			ft_find_error();
		ft_valid_char(c);
		nb_char = ft_count_tetri(c);
	}
	nb_tetri = (nb_char + 1) / 21;
	if (nb_char > (nb_tetri * 21) - 1)
		ft_find_error();
	if (close(fd) == -1)
		ft_find_error();
	return (nb_tetri);
}

void		ft_str_del(char **str)
{
	int		i;

	i = 0;
	while (str[i] != NULL)
	{
		free(str[i]);
		i++;
	}
	free(str[i]);
	free(str);
	str = NULL;
}
