/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetriminos.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cjacques <cjacques@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/08 10:35:29 by cjacques          #+#    #+#             */
/*   Updated: 2015/12/14 16:25:06 by cjacques         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static char		*ft_del_char(char *buf, char c)
{
	int		k;
	int		j;
	char	*tab;

	k = 0;
	j = 0;
	if ((tab = ft_memalloc(sizeof(tab) * 17)) == NULL)
		ft_find_error();
	while (k < 16)
	{
		if (buf[j] != c)
		{
			tab[k] = buf[j];
			k++;
		}
		j++;
	}
	tab[k] = '\0';
	return (tab);
}

static void		ft_schema(char schema[19][14])
{
	ft_strcpy(schema[0], "##..#...#");
	ft_strcpy(schema[1], "#...###");
	ft_strcpy(schema[2], "#...#..##");
	ft_strcpy(schema[3], "###...#");
	ft_strcpy(schema[4], "##..##");
	ft_strcpy(schema[5], "#...##...#");
	ft_strcpy(schema[6], "##.##");
	ft_strcpy(schema[7], "#..##..#");
	ft_strcpy(schema[8], "##...##");
	ft_strcpy(schema[9], "#...#...#...#");
	ft_strcpy(schema[10], "####");
	ft_strcpy(schema[11], "#...##..#");
	ft_strcpy(schema[12], "#..##...#");
	ft_strcpy(schema[13], "###..#");
	ft_strcpy(schema[14], "#..###");
	ft_strcpy(schema[15], "##...#...#");
	ft_strcpy(schema[16], "###.#");
	ft_strcpy(schema[17], "#...#...##");
	ft_strcpy(schema[18], "#.###");
}

char			**ft_file_to_tab(char *path, int nb)
{
	int		fd;
	int		ret;
	char	buf[BUF_SIZE + 1];
	char	**ptr;
	int		i;

	i = 0;
	fd = open(path, O_RDONLY);
	if (fd == -1)
		ft_find_error();
	ptr = ft_memalloc(sizeof(ptr) * (nb + 1));
	if (ptr == NULL)
		ft_find_error();
	while ((ret = read(fd, buf, BUF_SIZE)))
	{
		buf[ret] = '\0';
		ptr[i] = ft_del_char(buf, '\n');
		i++;
	}
	ptr[i] = NULL;
	if (close(fd) == -1)
		ft_find_error();
	return (ptr);
}

void			ft_verif_tetri(char **ptr, int nb)
{
	int		i;
	int		j;
	char	schema[19][14];

	i = 0;
	j = 0;
	ft_schema(schema);
	while (i < nb)
	{
		j = 0;
		while (j < 19)
		{
			if (ft_strstr(ptr[i], schema[j]) != NULL)
				break ;
			j++;
		}
		if (j == 19)
		{
			free(ptr);
			ft_find_error();
		}
		i++;
	}
}

void			ft_lst_del(t_piece **begin_lst)
{
	if (begin_lst == NULL || *begin_lst == NULL)
		return ;
	ft_lst_del(&(*begin_lst)->next);
	free(*begin_lst);
	*begin_lst = NULL;
}
