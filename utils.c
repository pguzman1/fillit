/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cjacques <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 16:21:17 by cjacques          #+#    #+#             */
/*   Updated: 2015/12/14 16:29:40 by cjacques         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		ft_put_in_board(t_piece tri, char **board, int i, int j)
{
	if (tri.first.x == 0 && tri.first.y == 0)
	{
		board[i + tri.first.x][j + tri.first.y] = tri.letter;
		board[i + tri.second.x][j + tri.second.y] = tri.letter;
		board[i + tri.third.x][j + tri.third.y] = tri.letter;
		board[i + tri.fourth.x][j + tri.fourth.y] = tri.letter;
	}
	else if (tri.first.x == 0 && tri.first.y == 1)
	{
		board[i][j] = tri.letter;
		board[i + tri.second.x][j + tri.second.y - 1] = tri.letter;
		board[i + tri.third.x][j + tri.third.y - 1] = tri.letter;
		board[i + tri.fourth.x][j + tri.fourth.y - 1] = tri.letter;
	}
	else
	{
		board[i][j] = tri.letter;
		board[i + tri.second.x][j + tri.second.y - 2] = tri.letter;
		board[i + tri.third.x][j + tri.third.y - 2] = tri.letter;
		board[i + tri.fourth.x][j + tri.fourth.y - 2] = tri.letter;
	}
}

static int		ft_is_possible(char **board, t_piece tri, int i, int j)
{
	int		len;

	len = ft_strlen(board[0]);
	if (tri.first.y == 0 && (tri.first.y + i >= len || tri.second.y + j >= len
			|| tri.third.y + j >= len || tri.fourth.y + j >= len
			|| tri.first.x + i >= len || tri.second.x + i >= len
			|| tri.third.x + i >= len || tri.fourth.x + i >= len))
		return (0);
	else if (tri.first.y == 1 && (j >= len
			|| tri.second.y + j - 1 >= len || tri.third.y + j - 1 >= len
			|| tri.fourth.y + j - 1 >= len || tri.first.x + i >= len
			|| tri.second.x + i >= len || tri.third.x + i >= len
			|| tri.fourth.x + i >= len))
		return (0);
	else if (tri.first.y == 2 && (j >= len
			|| tri.second.y + j - 2 >= len || tri.third.y + j - 2 >= len
			|| tri.fourth.y + j - 2 >= len || tri.first.x + i >= len
			|| tri.second.x + i >= len || tri.third.x + i >= len
			|| tri.fourth.x + i >= len))
		return (0);
	else if ((j == 0 && (tri.first.y == 1 || tri.first.y == 2))
			|| (j < 2 && tri.first.y == 2) || (!ft_is_empty(board, tri, i, j)))
		return (0);
	else
		return (1);
}

static void		ft_remove_piece(char **board, t_piece tri, int i, int j)
{
	if (tri.first.x == 0 && tri.first.y == 0)
	{
		board[i + tri.first.x][j + tri.first.y] = '.';
		board[i + tri.second.x][j + tri.second.y] = '.';
		board[i + tri.third.x][j + tri.third.y] = '.';
		board[i + tri.fourth.x][j + tri.fourth.y] = '.';
	}
	else if (tri.first.x == 0 && tri.first.y == 1)
	{
		board[i][j] = '.';
		board[i + tri.second.x][j + tri.second.y - 1] = '.';
		board[i + tri.third.x][j + tri.third.y - 1] = '.';
		board[i + tri.fourth.x][j + tri.fourth.y - 1] = '.';
	}
	else
	{
		board[i][j] = '.';
		board[i + tri.second.x][j + tri.second.y - 2] = '.';
		board[i + tri.third.x][j + tri.third.y - 2] = '.';
		board[i + tri.fourth.x][j + tri.fourth.y - 2] = '.';
	}
}

static int		ft_fill(char **board, t_piece *tri)
{
	int i;
	int j;
	int len;

	len = ft_strlen(board[0]);
	i = 0;
	if (tri == NULL)
		return (1);
	while (i < len)
	{
		j = 0;
		while (j < len)
		{
			if (ft_is_possible(board, *tri, i, j))
			{
				ft_put_in_board(*tri, board, i, j);
				if (ft_fill(board, tri->next))
					return (1);
				ft_remove_piece(board, *tri, i, j);
			}
			j++;
		}
		i++;
	}
	return (0);
}

char			**ft_solve(t_piece *tri, int *poss)
{
	int		i;
	char	**result;

	i = 0;
	result = NULL;
	while (poss[i] != 0)
	{
		result = ft_createboard(poss[i]);
		if (ft_fill(result, tri))
			return (result);
		i++;
	}
	return (result);
}
