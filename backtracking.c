/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backtracking.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cjacques <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/10 13:01:53 by cjacques          #+#    #+#             */
/*   Updated: 2015/12/14 17:14:21 by cjacques         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		**ft_createboard(int i)
{
	char	**board;
	int		j;
	int		k;

	j = 0;
	if (!(board = (char **)malloc(sizeof(board) * (i + 1))))
		ft_find_error();
	while (j < i)
	{
		board[j] = (char *)malloc(sizeof(board[j]) * (i + 1));
		if (!board[j])
			ft_find_error();
		j++;
	}
	board[j] = NULL;
	j = 0;
	while (j < i)
	{
		k = 0;
		while (k < i)
			board[j][k++] = '.';
		board[j][k] = '\0';
		j++;
	}
	return (board);
}

void		ft_printboard(char **board)
{
	int		i;
	int		j;
	int		len;

	i = 0;
	len = ft_strlen(board[0]);
	while (i < len)
	{
		j = 0;
		while (j < len)
		{
			ft_putchar(board[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

int			*ft_get_size(int n)
{
	int		*a;
	int		i;
	int		j;

	a = (int*)malloc(sizeof(a) * 4);
	if (!a)
		ft_find_error();
	a[0] = 0;
	a[1] = 0;
	a[2] = 0;
	a[3] = 0;
	j = 0;
	i = 0;
	while (j < 3 && i < 14)
	{
		if (((i * i) >= (n * 4)))
		{
			a[j++] = i;
		}
		i++;
	}
	return (a);
}

int			ft_is_empty(char **board, t_piece tri, int i, int j)
{
	if (tri.first.x == 0 && tri.first.y == 0
			&& board[i][j] == '.'
			&& board[tri.second.x + i][tri.second.y + j] == '.'
			&& board[tri.third.x + i][tri.third.y + j] == '.'
			&& board[tri.fourth.x + i][tri.fourth.y + j] == '.')
		return (1);
	else if (tri.first.x == 0 && tri.first.y == 1
			&& board[i][j] == '.'
			&& board[tri.second.x + i][tri.second.y + j - 1] == '.'
			&& board[tri.third.x + i][tri.third.y + j - 1] == '.'
			&& board[tri.fourth.x + i][tri.fourth.y + j - 1] == '.')
		return (1);
	else if (tri.first.x == 0 && tri.first.y == 2
			&& board[i][j] == '.'
			&& board[tri.second.x + i][tri.second.y + j - 2] == '.'
			&& board[tri.third.x + i][tri.third.y + j - 2] == '.'
			&& board[tri.fourth.x + i][tri.fourth.y + j - 2] == '.')
		return (1);
	else
		return (0);
}
